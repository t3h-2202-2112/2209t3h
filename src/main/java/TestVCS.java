import org.apache.commons.lang3.StringUtils;

public class TestVCS {

    //inner class
    static class test {

    }

    public static void main(String[] args) {
        System.out.println(new test());
    }

    static String tryCatchReturnDemo() {
        String s = "ZERO";

        try {
            s = s + "ONE";

            return s;
        } catch (Exception e) {
            s = s + "TWO";

            return s;
        } finally {
            s = s + "THREE";
        }
    }

    static void testFuncsVoidReturn() {
        //debug evaluate void testFuncsVoidReturn() -> undefined
        System.out.println();
        int i = 3;
        if (i < 4) {
            System.out.println("in return");
            return;
        } else {
            System.out.println(i);
        }
    }
}
