package db_demo;

import java.sql.*;

public class Runner {
    public static void main(String[] args) throws SQLException {
        // mariadb jdbc example
        // ket noi mariadb java

        //1 tao ket noi DB
        Connection connection = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3310/demo", // URL ket noi
                "root", "123"  // thong tin dang nhap
        );
        //Connection refused: db chua cai, sai username, pw, sai ten db, sai chuoi ket noi, db bi dong
        System.out.println("da ket noi data base ok ? " + !connection.isClosed());

        // doc tat ca du lieu tu bang employees
        /*
        employeeNumber int(11) not null primary key,
        lastName varchar(50),
        firstName  varchar(50),
        extension varchar(10),
        email varchar(10),
        officeCode varchar(10),
        reportTo varchar(10),
        jobTitle varchar(10)
         */
        // them mot ban ghi
        insertEmployee(connection);

        // trien khai code doc
        showEmployees(connection);


        connection.close();
    }

    private static void insertEmployee(Connection connection) throws SQLException{
        try (PreparedStatement statement = connection.prepareStatement("""
                INSERT INTO demo.employees
                (employeeNumber, lastName, firstName, extension, email, officeCode, reportTo, jobTitle)
                VALUES(4, 'nam', 'hong', NULL, '', '', NULL, NULL);
            """)) {
            System.out.println("them thanh cong id 4, so ban ghi: " + statement.executeUpdate());

        }
    }

    private static void showEmployees(Connection connection) throws SQLException {
        // script trong PreparedStatement: phai dung, chay truoc tren DB
        try (PreparedStatement statement = connection.prepareStatement("""
                    SELECT * from employees e;
                """)) {
            // tao bien luu luu tru ket qua truy van
            ResultSet resultSet = statement.executeQuery();
            // dung vong lap, lap tung ban ghi & in ra
            System.out.println("employeeNumber \t lastName \t firstName " +
                    "\t extension \t email \t officeCode" +
                    "\t jobTitle");
            while (resultSet.next()) {
                // c1 lay theo ten cot
                // c2 lay theo vi tri cot, tinh tu 1
                int employeeNumber = resultSet.getInt("employeeNumber");
                String lastName = resultSet.getString("lastName");
                String firstName = resultSet.getString("firstName");
                String extension = resultSet.getString("extension");
                String email = resultSet.getString("email");
                String officeCode = resultSet.getString("officeCode");
                String jobTitle = resultSet.getString("jobTitle");
                System.out.printf(
                        "%d \t %s \t %s \t %s \t %s \t %s \t %s \n",
                        employeeNumber, lastName, firstName,
                        extension, email, officeCode, jobTitle
                );
            }
        }
    }
}
