package collection_demo;

import array_demo.bai1.Student;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Runner {
    public static void main(String[] args) {
//        demoSet();
        handleDuplicateStudents();
    }

    private static void demoSet() {
        LinkedList<Double> myNumbers = new LinkedList<>();
        myNumbers.add(0.5D);
        myNumbers.add(1.5D);
        myNumbers.add(3.5D);
        myNumbers.add(4.5D);
        myNumbers.add(5.5D);
        System.out.println(myNumbers);
        System.out.println("pt dau: " + myNumbers.element());
        System.out.println("pt cuoi: " + myNumbers.getLast());
        System.out.println("poll: " + myNumbers.poll());
        System.out.println("LinkedList sau khi poll: " + myNumbers);
    }

    /**
     * mong muốn: khi thêm 2 student cùng id vào Set,
     * thì thêm student vị trí cuối/đầu
     */
    static void handleDuplicateStudents(){
        Student nam = new Student(1, "nam", null);
        Student nam2 = new Student(1, "tuyen", "cntt");
        Student nam3 = new Student(1, "nam3", null);
        Student nam4 = new Student(1, "nam4", null);
        Set<Student> students = new HashSet<>();
//        System.out.println(nam.equals(nam2));
        System.out.println("hash-code của 4 đối tượng cùng id 1");
        System.out.println(nam.hashCode());
        System.out.println(nam2.hashCode());
        System.out.println(nam3.hashCode());
        System.out.println(nam4.hashCode());
        System.out.println("So sánh eq theo điểm neo là thuộc tính id");
        System.out.println("so sánh ==: " + (nam == nam2));
        System.out.println("so sánh eq: " + nam.equals("nam2"));

        //hash code: dùng trong Set, khi gọi add()
        // -> so sánh theo cặp eq & hashcode



        //1 chỉ lấy đầu
        students.add(nam);
        students.add(nam2);
        students.add(nam3);// -> 37: 3 pt
        students.add(nam4);
        System.out.println("Set student sau khi ghi đè eq & hasc");
        System.out.println(students); //-> mong muon 1 pt

        // -> xu ly
        /*
         - duyet, lay sinh vien, so sanh
         - cách cổ điển
         for / while / iterator
         if
         add
         */


        //2 chỉ lấy cuối


    }
}
