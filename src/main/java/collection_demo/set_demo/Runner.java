package collection_demo.set_demo;

import demo_oop.Teacher;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Runner {
    public static void main(String[] args) {
        // list Teacher
//        createListTeacher();

        //set Teacher
//        createSetTeacher();

        //so sanh
        System.out.println(
                new Teacher("1", LocalDate.now(), false)
                        .equals(new Teacher("1", LocalDate.now(), true))
        );
    }

    /**
     * ko bao h chua null
     * ko can dung if ktra trung
     * -> bat buoc phai ghi de equals & hashCode
     */
    private static void createSetTeacher() {
        Set<Teacher> teacherSet = new HashSet<>();
        teacherSet.add(new Teacher("1"));
        teacherSet.add(new Teacher("1"));
        teacherSet.add(new Teacher("2"));
        System.out.println(teacherSet);
        for(Teacher teacher : teacherSet){
            System.out.println(teacher.hashCode() + " @ " + teacher);
        }
    }

    private static void createListTeacher() {
        // if else
        System.out.println(
                Arrays.asList(
                        new Teacher("1"),
                        new Teacher("2"),
                        new Teacher("1"),
                        null
                )
        );

    }
}
