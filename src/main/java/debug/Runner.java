package debug;

import java.time.LocalDateTime;

public class Runner {
    public static void main(String[] args) {
        double n1 = -4.5;
        double n2 = 3.9;
        double n3 = 2.5;

        printLargestNumber(n1, n2, n3);

        System.out.println("ket thuc");
    }

    private static void printLargestNumber(double n1, double n2, double n3) {
        if (n1 >= n2 && n1 >= n3) {
            System.out.println(n1 + " is the largest number.");
        } else if (n2 >= n1 && n2 >= n3) {
            System.out.println(n2 + " is the largest number.");
        } else {
            System.out.println(n3 + " is the largest number.");
        }

        printMyBrithDay();
    }

    private static void printMyBrithDay() {
        System.out.println(LocalDateTime.now());
    }
}
