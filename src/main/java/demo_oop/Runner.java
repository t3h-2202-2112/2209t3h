package demo_oop;

import java.time.LocalDate;

public class Runner {
    public static void main(String[] args) {
        Teacher jonh = new Teacher();
        jonh.setBirthDay(LocalDate.of(1990, 1, 1));
        System.out.println("Ngay sinh cua jonh:" + jonh.getBirthDay());
        System.out.println(jonh);
    }
}
