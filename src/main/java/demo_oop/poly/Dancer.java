package demo_oop.poly;

public class Dancer extends Person {
    private String groupName;

    public Dancer() {
    }

    public Dancer(String name, String designation, String groupName) {
        super(name, designation);
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void dancing() {
        System.out.println("dancing");
    }

    @Override
    public void learn() {
        System.out.println("Dancer đã ghi đè learn() bằng @Override, nên ko gọi learn() của ParentClass");
        System.out.println("Dancer is learning dace!");
    }

}
