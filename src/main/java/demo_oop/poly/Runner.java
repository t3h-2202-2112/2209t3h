package demo_oop.poly;

public class Runner {
    public static void main(String[] args) {
        Dancer dancer = new Dancer();

        // bieu hien da hinh
        // tôi là con người, đứng trên sân khấu tôi là dancer
        Person jack = new Dancer();
        jack.learn();
    }
}
