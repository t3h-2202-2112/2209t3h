package demo_oop.abstrction;

public class Runner {
    public static void main(String[] args) {
        GeometricShape geoCircle = new Circle();

        //class truu tuong ko tao thuc the bang tu khoa new new GeometricShape()
        // muon tao duoc thi phai trien khai class ben trong no
        GeometricShape geo = new GeometricShape() {
            @Override
            public double calculateArea() {
                return 0;
            }
        };
    }
}
