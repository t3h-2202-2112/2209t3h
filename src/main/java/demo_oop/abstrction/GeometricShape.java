package demo_oop.abstrction;

import java.awt.*;

public abstract class GeometricShape {
    private double a;
    private Color color;

    public abstract double calculateArea();
}
