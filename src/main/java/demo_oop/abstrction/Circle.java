package demo_oop.abstrction;

public class Circle extends GeometricShape{
    private double r;

    @Override
    public double calculateArea() {
        System.out.println("calc are circle");
        return r;
    }
}
