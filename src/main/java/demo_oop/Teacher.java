package demo_oop;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;
import java.util.List;

public class Teacher {
    // Fields
    private LocalDate birthDay;
    private String id; //-> ko duoc trung, neu trung, chi them gia tri
    private boolean sex;
    private List<String> major;

    public Teacher() {
    }

    public Teacher(String id) {
        this.id = id;
    }

    public Teacher(String id, LocalDate birthDay, boolean sex) {
        this.id = id;
        this.birthDay = birthDay;
        this.sex = sex;
    }

    public Teacher(LocalDate birthDay, String id, boolean sex,
                   List<String> major) {
        this.birthDay = birthDay;
        this.id = id;
        this.sex = sex;
        this.major = major;
    }

    // Setter
    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }
    // tuong tu voi 3 thuoc tinh con lai

    //Getter
    public LocalDate getBirthDay() {
        return birthDay;
    }
    // tuong tu voi 3 thuoc tinh con lai

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append(" ").append(birthDay);
        sb.append(", id=").append(id);
        sb.append(", ").append(sex);
        sb.append(", ").append(major);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null){
            return false;
        }

        if (this == o) {
            return true;
        }

        if (getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        return new EqualsBuilder().append(birthDay, teacher.birthDay)
                .append(id, teacher.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(birthDay).append(id).toHashCode();
    }
}
