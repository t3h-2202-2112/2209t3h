package demo_oop;

import java.time.LocalDate;
import java.util.List;

public class GeoTeacher extends Teacher {
    public GeoTeacher() {
    }

    /**
     * Hàm tạo 4 tham số cho lớp GetTeacher
     * Phải truyền hết 4 tham số
     * @param birthDay - Ngày tháng năm sinh yyyy-MM-dd
     * @param id - thẻ căn cước
     * @param sex - giới tính
     * @param major - danh sách ngành/môn giáo viên dạy
     */
    public GeoTeacher(LocalDate birthDay, String id, boolean sex,
                      List<String> major) {
        super(birthDay, id, sex, major);

    }
}
