package demo_oop.inhertance;

import demo_oop.GeoTeacher;
import demo_oop.MathTeacher;

import java.time.LocalDate;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        // Inheritance, constructor no params
        MathTeacher mathTeacher = new MathTeacher();
        mathTeacher.setBirthDay(LocalDate.of(1991, 2, 2));
        GeoTeacher geoTeacher = new GeoTeacher();
        geoTeacher.setBirthDay(LocalDate.of(1989, 12, 12));

        System.out.println(mathTeacher);
        System.out.println(geoTeacher);

        // Inheritance, constructor all param
        GeoTeacher hana = new GeoTeacher(
                LocalDate.of(1988, 5, 10),
                "1814525756132",
                true,
                List.of("Toán", "Sinh", "Lý")
        );
        System.out.println("Thuộc tính hana");
        System.out.println(hana);
    }
}
