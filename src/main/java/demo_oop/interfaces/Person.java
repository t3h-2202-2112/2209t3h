package demo_oop.interfaces;

import java.util.Arrays;

public class Person extends Animal implements ActionGroup {
    private String name;

    @Override
    public void learn(String[] something) {
        System.out.println("learning: " + Arrays.asList(something));
    }

    @Override
    public void walk(long km) {
        System.out.printf("i'll walk %d km", km);
    }

    @Override
    public void eat(String food) {
        System.out.println("i'm eating " + food);
    }

    @Override
    public void sleep() {
        System.out.println("person sleep");
    }

    // method truu tuong cua animal - trien khai no
    @Override
    public void seeing() {
        System.out.println("person see sth");
    }
}
