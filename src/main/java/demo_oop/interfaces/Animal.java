package demo_oop.interfaces;

/**
 * abstract đại dịên cho class trừu tượng
 * khai báo method() đầy đủ
 * khai báo được method() giống interface -> method truu tuong
 */
public abstract class Animal {

    // method trừu tơựng - giong interfave
    public abstract void seeing();

    // method day du
    public void sing(){
        System.out.println("animal singing");
    }

}
