package demo_oop.interfaces;

import java.awt.*;
import java.util.Arrays;

public class Dog implements ActionGroup {
    private String name;
    private Color eyesColor;


    public Dog(String name, Color eyesColor) {
        this.name = name;
        this.eyesColor = eyesColor;
    }

    @Override
    public void learn(String[] something) {
        System.out.println("learning: " + Arrays.asList(something));
    }

    @Override
    public void walk(long km) {
        System.out.printf("i'll walk %d km", km);
    }

    @Override
    public void eat(String food) {
        System.out.println("i'm eating " + food);
    }

    @Override
    public void sleep() {
        System.out.println("dog sleep");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("name='").append(name).append('\'');
        sb.append(", eyesColor=").append(eyesColor);
        sb.append('}');
        return sb.toString();
    }
}
