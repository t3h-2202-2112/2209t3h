package demo_oop.interfaces;

// human implament PersonRunable extends ActionGroup
public class Human implements PersonRunable, PersonSpeakable {
    @Override
    public void learn(String[] something) {

    }

    @Override
    public void walk(long km) {

    }

    @Override
    public void eat(String food) {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void personRunning() {

    }

    @Override
    public void personSpeaking() {

    }
}
