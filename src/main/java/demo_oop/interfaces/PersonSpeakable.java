package demo_oop.interfaces;

public interface PersonSpeakable {
    void personSpeaking();

    void personRunning();
}
