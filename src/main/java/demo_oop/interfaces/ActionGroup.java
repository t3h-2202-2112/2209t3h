package demo_oop.interfaces;

public interface ActionGroup {
    // định nghĩa ra bộ khung hành động
    // class mà triển khai từ interface bắt buộc phải triển khai
    // những hành động trong interface
    // cách lập trình theo interface
    void learn(String[] something);

    void walk(long km);

    void eat(String food);

    void sleep();

}
