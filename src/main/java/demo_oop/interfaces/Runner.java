package demo_oop.interfaces;

public class Runner {
    public static void main(String[] args) {
        Person haha = new Person();
        haha.learn(new String[]{"java", "C++", "js", "math"});
        haha.eat("cake");
        haha.walk(100L);
    }
}
