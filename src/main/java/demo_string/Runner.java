package demo_string;

public class Runner {
    public static void main(String[] args) {
//        bai2();
//        bai3();
        //EPOC TIME
        System.out.println(System.currentTimeMillis());


    }

    static void bai2() {
        long start = System.currentTimeMillis();
        StringBuilder builder = new StringBuilder("");
        for (int in = 0; in < 500_000; in++) {
            builder.append("a");
        }
        System.out.printf("them 500k StringBuilder: %d ms",
                (System.currentTimeMillis() - start));
    }
    static void bai3() {
        long start = System.currentTimeMillis();
        String myText = "";
        for (int in = 0; in < 500_000; in++) {
            myText += "a";
        }
        System.out.printf("them 500k StringBuilder: %d ms",
                (System.currentTimeMillis() - start));
    }
}
