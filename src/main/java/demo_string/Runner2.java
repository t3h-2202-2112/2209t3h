package demo_string;

import java.util.Arrays;
import java.util.List;

public class Runner2 {
    public static void main(String[] args) {
//        bai6();
//        bai8();
        Bai2();
    }

    /**
     * In ra cac ki tu dac biet trong chuoi
     */
    public static void Bai2() {
        String str = "Quang_@^&^#%2023.";
        for (int i = 0; i < str.length(); i++) {
            char myChar = str.charAt(i);
            if (!(myChar >= 'a' && myChar <= 'z')
                    || !(myChar >= 'A' && myChar <= 'Z')) {
                System.out.print(myChar);
            }
        }
    }

    private static void bai8() {
        List<String> myWords = Arrays.asList(
                "sadsa", null, "",
                "sadas3432", "                    ", "\r\t\t",
                "584545456413", "", ("" + "43534534")
        );
        int max = 0;
        int target = 0;
        for (int index = 0; index < myWords.size(); index++) {
            String text = myWords.get(index);
            if (text == null || "".equals(text.trim())) {
                continue;
            }

            if (text.length() > max) {
                max = myWords.get(index).length();
                target = index;
            }
        }
        System.out.printf("Longest word: %s", myWords.get(target));
        System.out.printf("\nLength of the longest word: %d", max);
    }

    static void bai6() {
        String text1 = "ha";
        String text2 = "";
        String text3 = "halo";
        String text4 = null;
        String text5 = "       ";

        System.out.println(addPostFix(text1));
        System.out.println(addPostFix(text2));
        System.out.println(addPostFix(text3));
        System.out.println(addPostFix(text4));
        System.out.println(addPostFix(text5));
    }

    static String addPostFix(String input) {
        if (input == null) {
            return "";
        }
        if ("".equals(input.trim())) {
            return "";
        }
        if (input.length() < 3) {
            return input + "ly";
        }

        return input + "ing";
    }
}
