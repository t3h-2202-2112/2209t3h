package array_demo.bai1;

import java.time.LocalDate;
import java.util.Objects;

public class Student {
    private int id;
    private String name;
    private String major;
    private LocalDate birthDay;

    public Student(int id, String name, String major) {
        this.id = id;
        this.name = name;
        this.major = major;
    }

    public Student(int id, String name, String major, LocalDate birthDay) {
        this.id = id;
        this.name = name;
        this.major = major;
        this.birthDay = birthDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name);
        sb.append(", major='").append(major);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (anotherObject == null) {
            return false;
        }
        if (this == anotherObject) { // == bắt buộc phải ghi đè hashCode()
            return true;
        }
        if (getClass() != anotherObject.getClass()) {
            return false;
        }
        Student student = (Student) anotherObject;
        if(student.getId() <= 0){
            return false;
        }

        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
