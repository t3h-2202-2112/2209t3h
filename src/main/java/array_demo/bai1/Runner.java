package array_demo.bai1;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Runner {

    static Student[] students;

    public static void main(String[] args) {
        //z
        Student sv = new Student(1, "tran", "cntt");
        Student sv2 = new Student(2, "nguyen", "y");
        Student sv3 = new Student(3, "hong", "ketoan");
        Student sv4 = new Student(4, "nam", "endz");
        Student sv5 = new Student(5, "tien", "toan");
        Student sv6 = new Student(6, "hanh", "gv");
        students = new Student[]{sv, sv2, sv3, sv4, sv5, sv6};
        //y


        //z
        for (int index = 0; index < students.length - 1; index++) {
            for (int sub = 0; sub < students.length - index - 1; sub++) {
                // compareTo -1 0 1
                //
                if (students[sub].getName()
                        .compareTo(students[sub + 1].getName()) < 1) {
                    // swap students[j+1] and students[j]
                    Student temp = students[sub];
                    students[sub] = students[sub + 1];
                    students[sub + 1] = temp;
                }
            }
        }
        System.out.println("After sort by Name alphabet");
        System.out.println(Arrays.toString(students));

        //1a
        Student sv7 = new Student(6, "hoang", "gv");
        Student sv8 = new Student(6, "nhung", "tin");
        Student sv9 = new Student(9, "lan han", "td");
        System.out.println("ds student trc khi them: " + students.length);
        students = addNewStudent(sv7, sv8, sv9); // chinh xac la them sv9
        System.out.println("ds student sau khi them: " + students.length);

        //3a & 6a
        // them tuoi cho sv
        Random myRandomMonth = new Random();
        Random myRandomDay = new Random();
        Random myRandomYear = new Random();
        for (Student student : students) {
            student.setBirthDay(LocalDate.of(
                    myRandomYear.nextInt(1994, 2003),
                    myRandomMonth.nextInt(12),
                    myRandomDay.nextInt(29)));
        }

        //tinh tuoi den hien tai
        LocalDate currentDate = LocalDate.now();
        for (Student student : students) {
            Period calcYear = Period.between(student.getBirthDay(), currentDate);
            System.out.printf("%s - Tuoi: %d.%d \n",
                    student,
                    calcYear.getYears(),
                    calcYear.getMonths() + 1);
        }
    }

    /**
     * Student... -> varrags
     *
     * @param inputStudents
     * @return new list students
     */
    static Student[] addNewStudent(Student... inputStudents) {
        Student[] newStudentsTemp = new Student[10];
        // ktra ds student trong input
        int tempIndex = 0;
        Set<Integer> idsAdded = new HashSet<>();
        for (Student newStudent : inputStudents) {
            boolean isNotDuplicate = true;

            for (Student mainStudent : students) {
                int newStudentId = newStudent.getId();
                // nếu id của student mới ko chứa trong ds cũ
                if (newStudentId == mainStudent.getId()) {
                    isNotDuplicate = false;
                    break;
                }
            }

            if (isNotDuplicate) {
                newStudentsTemp[tempIndex] = newStudent;
                tempIndex++;
            }
        }
        System.out.println("So luong sv dc them: " + tempIndex); // tempIndex -> 1
        // tao mang moi & return mang sv moi

        Student[] result = new Student[students.length + tempIndex];
        if (result.length == students.length) {
            return students;
        }

        for (int index = 0; index < result.length; index++) {
            if (index == students.length) {
                result[index] = newStudentsTemp[tempIndex - 1];
            } else {
                result[index] = students[index];
            }
        }

        return result;
    }
}
