package array_demo;

import demo_oop.interfaces.Dog;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
//        ex0();
//        increSize();
//        firstArrayList();
        hello();
    }

    /**
     * Khai báo & in mảng thuần
     */
    static void ex0() {
        int myNumber[] = new int[2];
        int[] myNumber2 = new int[]{1, 5, 6};
        // lỗi đỏ xảy ra trong quá trình biên dịch
        // error compile time
        System.out.println(myNumber2);
        System.out.println(myNumber);

        //c1 loop
        for (int item : myNumber) {
            System.out.print(item + " ");
        }
        System.out.println();
        //c2 convert về Arrays.toString
        System.out.println(Arrays.toString(myNumber));
    }

    static void increSize() {
        //newCapacity = oldCapacity + (oldCapacity >> 1)
        int oldCapacity = 20;
        //(oldCapacity >> 1): convert oldCapacity -> binary -> dịch tất cả các bít sang phải 1 đơn vị
        System.out.printf("newCapacity: %d", oldCapacity + (oldCapacity >> 1));
    }

    static void firstArrayList() {
        // khai bao
        List<Dog> dogges = new ArrayList<>();
        dogges.add(new Dog("phu quoc", Color.BLACK));
        dogges.add(new Dog("pug", Color.DARK_GRAY));
        dogges.add(new Dog("ta", Color.pink));
        dogges.add(new Dog("chow", Color.CYAN));
        dogges.add(new Dog("nga", Color.MAGENTA));

        System.out.println(dogges);

    }

    static void hello()
    {


          int xxx[] = {1, 3, 3, 8, 9};
        int xx[] = new int[xxx.length];

          xx[0] = xxx[0];
        int aaaa = 1; //đánh dấu độ dài mảng kq
          for (int index = 1;index < xxx.length;index++) {

              for (int indexx= aaaa-1;indexx< xxx.length; indexx++) {
                if (xxx[index] == xx[indexx])                     break;

                xx[aaaa]=xxx[index];
                aaaa++;

            }
        }
        for(int hihi = 0; hihi<aaaa; hihi ++)
        {
            System.out.print(xx[hihi] + " ");
        }


    }
}
