package javaio_demo;

import java.io.File;

/**
 * Demo class File
 */
public class Runner {
    public static void main(String[] args) {
        //  trên window slash \x -> biểu thị kí tự đặc biệt trong java
        // -> unescape: dùng 2 dấu \\
        //application is allowed: hiểu là có thể chạy file đó trên cmd
        File myFile = new File("D:\\java\\test.txt");
        File myFile2 = new File("/home/ugo/my-ide/GoLand/bin/inspect.sh");
        System.out.println("Print ab file info");

        //demo đường dẫn tương đối - relative
        // relative -> gắn vị trí đứng khi đọc file đó
        // đường dẫn base: -> {PROJECT_ROOT} path
        File myRelativeFile = new File("../2209t3h/README.md"); //-> di chuyển đến 1 file ngoài dự án
        System.out.println("Print relative file");

        File mRFile2 = new File("src/main/java/TestVCS.java");
        System.out.println("my RFile 2 existed ? " + mRFile2.exists());

        // folder & file name: phân biệt chữ hoa / thường
        // linux / macos: co phan biet
        // window ?

    }
}
