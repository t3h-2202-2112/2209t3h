package javaio_demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class RunnerReadFile {
    static String fileName = "dataInputStream.txt";
    public static void main(String[] args) throws IOException {
        /** Một số lỗi thường gặp khi xử lý file -> ném lỗi IOException - checked
         * 1, File ko tồn tại
         * 2, Sai đường dẫn
         * 3, Chương trình ko có quyền đọc file đó(file bị khoá, file chỉ có quyền ghi)
         * 4, Chương trình ko ko có quyền truy cập vào đường dẫn
         */
        // doc file pom.xml trong du an
        FileInputStream fileInputStream = new FileInputStream(fileName);

        System.out.println("Byte chưa đọc: " + fileInputStream.available());
        // read() đọc 1 kí tự khi gọi
        System.out.println("đọc byte đầu tiên, kí  tự " + (char) fileInputStream.read());

        System.out.printf("số lượng byte còn lại trong file [%s]: %d",
                fileName, fileInputStream.available());

        // đọc hết file -> dùng while
        int c;
        // gán trong biểu thức điều kiên ( )
        while ((c = fileInputStream.read()) != -1) {
            System.out.print((char) c);
        }
        fileInputStream.close();
    }

    //cach 2 dung try-catch
    static void readFileUsingTryCatch() {
        //try(statement implement Closeable){} -> tính năng mới từ phiên bản java 7, ko cần gọi hàm close()
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            System.out.println("Byte chưa đọc: " + fileInputStream.available());
            // read() đọc 1 kí tự khi gọi
            System.out.println("đọc byte đầu tiên, kí  tự " + (char) fileInputStream.read());
            System.out.printf("số lượng byte còn lại trong file [%s]: %d",
                    fileName, fileInputStream.available());
            // đọc hết file -> dùng while
            int c;
            // gán trong biểu thức điều kiên ( )
            while ((c = fileInputStream.read()) != -1) {
                System.out.print((char) c);
            }
        } catch (IOException e) {
            System.out.printf("quá trình đọc file %s lỗi", fileName);
        }


    }
}
