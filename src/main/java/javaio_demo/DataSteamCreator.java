package javaio_demo;

import java.io.*;

public class DataSteamCreator {
    static String MY_FILE = "myDataOut.txt";

    public static void main(String[] args) {
//        crateDataAsRawByte();

        //đọc file byte
        try (DataInputStream inputStream = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(MY_FILE)))) {
            System.out.println("byte:" + inputStream.readBoolean());
            System.out.println("short:" + inputStream.readDouble());
//            System.out.println("integer:" + inputStream.readInt());
//            System.out.println("long:" + inputStream.readLong());
//            System.out.println("float:" + inputStream.readFloat());
//            System.out.println("double:" + inputStream.readDouble());
//            System.out.println("boolean:" + inputStream.readBoolean());
//            System.out.println("boolean 2:" + inputStream.readBoolean());


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void crateDataAsRawByte() {
        try (DataOutputStream out = new DataOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream(MY_FILE)))) {
            out.writeByte(100);
            out.writeShort(0xFFFA);  // -1
            out.writeInt(0xABCD);
            out.writeLong(0x114_5678);
            out.writeFloat(18.24f);
            out.writeDouble(77.66);
            out.writeBoolean(true);
            out.writeBoolean(false);

            out.flush();
            // DataOutputStream đ
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
