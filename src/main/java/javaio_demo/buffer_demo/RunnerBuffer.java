package javaio_demo.buffer_demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalTime;

/**
 * Dùng buffer chia & đọc file theo block: 4 8 16 32 128 ...
 */
public class RunnerBuffer {
    static String imageInputPath = "/mnt/sda1/download/javaio_demo/bigImage-20MB.jpg";
    static String imageOutputPath = String.format("/mnt/sda1/download/javaio_demo/bigImage-20MB-2-BufferRaw_%s.jpg",
            (LocalTime.now().toString()).replace(":","_"));

    public static void main(String[] args) {
        long end;
        long start = System.currentTimeMillis();
        //tao bo dem byte
        byte[] rawBuffer = new byte[4000]; //-> 8KB
        // cach viet cu
        try (FileOutputStream outputStream = new FileOutputStream(imageOutputPath);
             FileInputStream fileInputStream = new FileInputStream(imageInputPath);) {
            int byteReader;
            while ((byteReader = fileInputStream.read(rawBuffer)) != -1) {
                // ghi ra output stream
                //đọc mỗi lần 4 KiB
                outputStream.write(rawBuffer, 0, byteReader);
            }

        } catch (IOException ioe) {
            System.out.println("error read image file");
        }
        System.out.println("copy file het:" + (System.currentTimeMillis() - start));
        // ko buffer 42s -> 42_000
        // buffer 4k  -> 39 ms; up 1076
        // buffer 8k  -> 34 ms; up 1235
        // buffer 16k  -> 25 ms; 1680
        // buffer 256k  -> 23 ms; 1826
        // buffer 512k  -> 15 ms; size > 128 time 4k; 2800
        // buffer 1M  -> 18 ms
        // buffer 1.5M  -> 25 ms
        // buffer 2M  -> 22 ms
        // suy ra cong thuc
        // trade-off: khi càng tăng buffer đến mưc nhất định
    }
}
