package javaio_demo.buffer_demo;

import java.io.*;
import java.time.LocalTime;

/**
 * Dùng BufferedInputStream -> để đọc & copy file
 */
public class RunnerBuffer2 {
    static String imageInputPath = "/mnt/sda1/download/javaio_demo/bigImage-20MB.jpg";
    static String imageOutputPath = String.format("/mnt/sda1/download/javaio_demo/bigImage-20MB-BufferedInputStream_%s.jpg",
            (LocalTime.now().toString()).replace(":","_"));

    public static void main(String[] args) {
        bufferedReadPerByte();
        bufferedReadBlockByte();
    }

    private static void bufferedReadBlockByte() {
        long end;
        long start = System.currentTimeMillis();
        // cach viet cu
        // đọc từng byte của FileInput/OutputStream: 42s
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(imageInputPath));
             BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(imageOutputPath))) {
            int byteReader;
            byte[] block =  new byte[512_000];
            while ((byteReader = bufferedInputStream.read(block)) != -1) {
                // ghi ra output stream
                //đọc mỗi lần 4 KiB
                outputStream.write(block, 0, byteReader);
            }

        } catch (IOException ioe) {
            System.out.println("error read image file");
            System.out.println(ioe.getMessage());
        }
        System.out.println("BufferedInput/OutputStream block 512_000, copy file het:" + (System.currentTimeMillis() - start));

        //42s 318 ms : 130
    }

    private static void bufferedReadPerByte() {
        long end;
        long start = System.currentTimeMillis();
        // cach viet cu
        // đọc từng byte của FileInput/OutputStream: 42s
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(imageInputPath));
             BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(imageOutputPath))) {
            int byteReader;
            while ((byteReader = bufferedInputStream.read()) != -1) {
                // ghi ra output stream
                //đọc mỗi lần 4 KiB
                outputStream.write(byteReader);
            }

        } catch (IOException ioe) {
            System.out.println("error read image file");
            System.out.println(ioe.getMessage());
        }
        System.out.println("BufferedInput/OutputStream copy file het:" + (System.currentTimeMillis() - start));

        //42s 318 ms : 130
    }
}
