package javaio_demo.buffer_demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class RunnerWithoutBuffer {
    // file info
    static String imageInputPath = "/mnt/sda1/download/bigImage-20MB.jpg";
    static String imageOutputPath = "/mnt/sda1/download/bigImage-20MB-2-NoBuffer.jpg";

    public static void main(String[] args) {
        long end;
        long start = System.nanoTime();
        FileOutputStream outputStream = null;
        FileInputStream fileInputStream = null;
        // cach viet cu
        try {
            outputStream = new FileOutputStream(imageOutputPath);
            fileInputStream = new FileInputStream(imageInputPath);
            int byteReader;
            while ((byteReader = fileInputStream.read()) != -1) {
                // ghi ra output stream
                outputStream.write(byteReader);
            }

        } catch (IOException ioe) {
            System.out.println("error read image file");
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException ioe2) {

            }
            System.out.println("copy file het:" + (System.nanoTime() - start) );
        }

        //12 core, 3.6, 3500, 32 -> 42s
//        https://raw.githubusercontent.com/samdutton/simpl/gh-pages/bigimage/bigImage-20MB.jpg
    }
}
