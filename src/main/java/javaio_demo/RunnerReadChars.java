package javaio_demo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class RunnerReadChars {
    static String MY_FILE = "dataInputStream.txt";

    public static void main(String[] args) {
        try (InputStreamReader isr = new InputStreamReader(
                new FileInputStream(MY_FILE), StandardCharsets.UTF_8)) {

            int req;
            System.out.println("character encoding: " + isr.getEncoding());
            List<String> lines = new ArrayList<>();
            StringBuilder myline = new StringBuilder();
            while ((req = isr.read()) != -1) {
                char myChar = (char) req;
                if (myChar == 32) {// ki tu xuong doong
                    lines.add(myline.toString());
                    myline = new StringBuilder();
                } else {
                    myline.append(myChar);
                }
            }
            System.out.println(lines);
        } catch (IOException e) {
            System.out.println("loi doc file: "  + e.getMessage());
        }
    }
}
