package exception_demo;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class BaiTap {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//        demobt1();
        Object o = new Object();
        String test = (String) o;
    }

    private static void demobt1() {
        int ngay, thang, nam;
        System.out.print("nhap ngay - thang - nam: ");
        try {
            ngay = scanner.nextInt();
            if (ngay < 0 || ngay > 31) {
                //ném lỗi
                System.out.println("lỗi ngay");
                throw new IllegalArgumentException("ngay ko hop le ");
            }
            thang = scanner.nextInt();
            if (thang < 0 || thang > 12) {
                //ném lỗi
                System.out.println("lỗi thang");
                throw new IllegalArgumentException("thang ko hop le ");
            }
            nam = scanner.nextInt();
            if (nam < 1990 || nam > LocalDate.now().getYear()) {
                //ném lỗi
                System.out.println("lỗi nam");
                throw new IllegalArgumentException("ngay ko hop le ");
            }

            System.out.println("input ok");
        } catch (IllegalArgumentException | InputMismatchException iae) {
            // dấu | BẮT NHIỀU EXCEPTION CÙNG CẤP
            throw new IllegalArgumentException("ngay, thang, nam nhap ko hop le " + iae.getMessage());
        }
    }
}
