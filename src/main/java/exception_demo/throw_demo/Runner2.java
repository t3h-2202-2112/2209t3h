package exception_demo.throw_demo;

import java.io.File;
import java.io.IOException;

public class Runner2 {
    public static void main(String[] args) {
        //throw xử checked
        // nếu ko xử lý thì lỗi tiếp tục xuất hiện
        // try catch
        try {
            throwGetItemOutOfBound3();
        } catch (IndexOutOfBoundsException ioed) {
            System.out.println(ioed.getMessage());
        } catch (IOException e) {
            System.out.println("đã bắt IOExcedption ử main");
        }
    }

    //throw IOException: đã throw checked trong body method, bắt buộc phải ném lên tên hàm
    // throws được dùng trên tên hàm lúc khai báo
    static void throwGetItemOutOfBound() throws IOException {
        try {
            System.out.println("demo throw checked exception");
            throw new IOException("demo throw checked exception");
        } finally {
            System.out.println("đã ném lên tầng trên");
        }
    }

    static void throwGetItemOutOfBound2() throws IOException {
        throwGetItemOutOfBound();
    }

    static void throwGetItemOutOfBound3() throws IOException {
        throwGetItemOutOfBound2();
    }
}
