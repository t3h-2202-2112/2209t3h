package exception_demo.throw_demo;

public class Runner {
    static int[] array = {2, 2, 1};

    public static void main(String[] args) {
        //throw xử unchecked
        // nếu ko xử lý thì lỗi tiếp tục xuất hiện
        // try catch
        try {
            throwGetItemOutOfBound2();
        } catch (IndexOutOfBoundsException ioed) {
            System.out.println(ioed.getMessage());
        }
    }

    //throw chia cho 0 / index out of bound
    // ném vào hàm main xử  lý - cách này: ném lên tầng trên để xử lý
    static void throwGetItemOutOfBound() {
        try {
            int item = array[-1];
        } catch (IndexOutOfBoundsException ioed) {
            throw new IndexOutOfBoundsException("Unchecked: lỗi lấy phần tử ngoài phạm vi mảng: " + ioed.getMessage());
        }
    }

    static void throwGetItemOutOfBound2() {
        throwGetItemOutOfBound();
    }

    static void throwGetItemOutOfBound3() {
        throwGetItemOutOfBound2();
    }
}
