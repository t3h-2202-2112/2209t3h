package exception_demo;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BaiTap2 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // tao constant LocalDate
        LocalDate myBrithDay = LocalDate.of(1999, Month.AUGUST, 10);
        System.out.println("nhap thoi gian tinh: ");
//        int day = scanner.nextInt();
//        int month = scanner.nextInt();
//        int year = scanner.nextInt();
//        LocalDate endDate = LocalDate.of(year, month, day);
        // tao ngay ket thuc
        LocalDate endDate = LocalDate.of(2002, 1, 12);
        System.out.printf("Khoang cach 2 date: %s -> %s %n", myBrithDay, endDate);
        Period calculateAge = Period.between(myBrithDay, endDate);
        //nếu kết quả < 0 : -> ném ra lỗi AgeException
        if (calculateAge.getYears() < 0
                || calculateAge.getMonths() < 0
                || calculateAge.getDays() < 0) {
            //custom exception
            throw new AgeException(
                    String.format("khong tinh duoc tuoi, thoi gian end(%s) truoc ngay sinh(%s)",
                            endDate, myBrithDay)
            );
        }

        System.out.printf("so tuoi: %d nam %d thang %d ngay",
                calculateAge.getYears(), calculateAge.getMonths(), calculateAge.getDays());

    }

}
