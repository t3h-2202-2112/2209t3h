package exception_demo;

/**
 * DEV muốn tạo một loại lỗi để định nghĩa 1 lỗi về nghiệp vụ trong hệ thống
 */
public class AgeException extends RuntimeException {
    public AgeException(String format) {
        super(format);
    }
}
